package org.academiadecodigo.bootcamp.producerconsumerqueue;

import org.academiadecodigo.bootcamp.producerconsumerqueue.bqueue.BQueue;

/**
 * Produces and stores integers into a blocking queue
 */
public class Producer implements Runnable {

    private final BQueue<Integer> queue;
    private int elementsToProduce;

    /**
     * @param queue the blocking queue to add elements to
     * @param elementsToProduce the number of elements to produce
     */
    public Producer(BQueue<Integer> queue, int elementsToProduce) {
        this.queue = queue;
        this.elementsToProduce = elementsToProduce;
    }

    @Override
    public void run() {

        for (int i = 1; i < elementsToProduce; i++) {

            synchronized (queue) {

                queue.offer(i);
                System.out.println(Thread.currentThread().getName() + " added element " + i + " to the queue.");

                if (queue.getLimit() == queue.getSize()) {
                    System.out.println(Thread.currentThread().getName() + " has left the queue full.");
                }
            }

            try {
                Thread.sleep((int) (Math.random() * 10));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
