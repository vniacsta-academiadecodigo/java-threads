package org.academiadecodigo.bootcamp.producerconsumerqueue;

import org.academiadecodigo.bootcamp.producerconsumerqueue.bqueue.BQueue;

/**
 * Consumer of integers from a blocking queue
 */
public class Consumer implements Runnable {

    private final BQueue<Integer> queue;
    private int consumedElements;

    /**
     * @param queue the blocking queue to consume elements from
     * @param consumedElements the number of elements to consume
     */
    public Consumer(BQueue<Integer> queue, int consumedElements) {
        this.queue = queue;
        this.consumedElements = consumedElements;
    }

    @Override
    public void run() {

        for (int i = 1; i < consumedElements; i++) {

            // this lock is to make sure the prints have the right value
            synchronized (queue) {

                int removedEl = queue.poll();
                System.out.println(Thread.currentThread().getName() + " has consumed element "
                        + removedEl + " from the queue.");

                if (queue.getSize() == 0) {
                    System.out.println(Thread.currentThread().getName() + " has left the queue empty.");
                }
            }

            try {
                Thread.sleep((int) (Math.random() * 10));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }


    }
}
