package org.academiadecodigo.bootcamp.producerconsumerqueue;

import org.academiadecodigo.bootcamp.producerconsumerqueue.bqueue.BQueue;

public class Main {

    public static void main(String[] args) {

        BQueue<Integer> queue = new BQueue<>(10);

        Producer p1 = new Producer(queue,8);
        Thread t1 = new Thread(p1);
        t1.setName("Producer Bruce");

        Producer p2 = new Producer(queue,14);
        Thread t2 = new Thread(p2);
        t2.setName("Producer Jackson");

        Consumer c1 = new Consumer(queue, 20);
        Thread t3 = new Thread(c1);
        t3.setName("Consumer Mary");

        Consumer c2 = new Consumer(queue, 16);
        Thread t4 = new Thread(c2);
        t4.setName("Consumer Samantha");

        t3.start();
        t4.start();

        t1.start();
        t2.start();

    }

}

